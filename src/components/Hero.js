import Car from "./images/img_car.png";
const Hero = () => {
  return (
    <section className="container-flex hero">
      <div className="row">
        <div className="col-lg-6 col-md-12 d-flex heroSection">
          <div className="container row justify-content-sm-start align-self-center sewaCopy">
            <h1 className="mb-3 ">
              {"Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)"}
            </h1>
            <p className="mb-3">
              {
                "Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani               kebutuhanmu untuk sewa mobil selama 24 jam."
              }
            </p>
            <a href={"/cars"}><button class="btn btn-green" >Mulai Sewa Mobil</button></a>
          </div>
        </div>
        <div className="col-lg-6 col-md-12 sm-remove">
          <img src={Car} alt="Round Illustration" id="hero-fg" />
        </div>
      </div>
    </section>
  );
};

export default Hero;
