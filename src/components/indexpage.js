import Navbar from "./Navbar";
import Hero from "./Hero";
import Service from "./Service";
import WhyUs from "./WhyUs"
import Started from "./Started"
import Faq from "./Faq"
import Footer from "./Footer";


const Page = () => {
    

  return (
    <div className="container">
      <Navbar />

      <Hero />
      <Service/>
      <WhyUs/>
      <Started/>
      <Faq/>
      <Footer/>
    </div>
  );
}

export default Page;