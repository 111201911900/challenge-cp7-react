const Started = () => {
  return (
    <div class="started mb-md-0 mb-5 mt-5">
      <div class="container">
        <div class="started-content text-center">
          <div class="row">
            <div class="col-md-8 mar">
                <h1>Sewa Mobil di (Lokasimu) Sekarang</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <a href={"/cars"}><button class="btn btn-green" >Mulai Sewa Mobil</button></a>
            </div>
          </div>
        </div>
      </div>         
    </div>
  );
};

export default Started;
