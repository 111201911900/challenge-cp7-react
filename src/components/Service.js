import Women from "./images/img_service.png";
const Service = () => {
  return (
    <div class="service mt-5" id="services">
      <div class="container">
        <div class="row main">
          <div class="col-md-6">
            <img src={Women} alt="mbak" width="100%"/>
          </div>
          <div class="col-md-6 text-left">
              <h1>
                Best Car Rental for any kind of trip in (Lokasimu)!
              </h1>
              <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
                 kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
              <ul class="listCentang">
               <li>Sewa Mobil Dengan Supir di Bali 12 Jam</li>
               <li>Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
               <li>Sewa Mobil Jangka Panjang Bulanan</li>
               <li>Gratis Antar - Jemput Mobil di Bandara</li>
               <li>Layanan Airport Transfer / Drop In Out</li>
              </ul>
          </div>        
        </div>
      </div>
    </div>
  );
};

export default Service;
