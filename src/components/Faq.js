const Faq = () => {
  return (
    <div class="question" id="faq">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mt-md-0 mt-5">
            <h1>Frequently Asked Question</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

          </div>
          <div class="col-md-6">
            <div class="accordion accordion-flush" id="accordionFlushExample">
              <div class="accordion-item mb-3 mb-3" data-aos="fade-left">
                  <h2 class="accordion-header" id="flush-headingOne">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                          data-bs-target="#flush-collapseOne" aria-expanded="false"
                          aria-controls="flush-collapseOne">
                          Apa saja syarat yang dibutuhkan?
                      </button>
                  </h2>
                  <div id="flush-collapseOne" class="accordion-collapse collapse"
                      aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body">Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                          Veniam, tenetur, iure sapiente doloremque minus quas ipsam in nostrum suscipit saepe
                          voluptate perferendis provident, ipsa explicabo! Neque sapiente quisquam corporis
                          fugiat.</div>
                  </div>
              </div>
              <div class="accordion-item mb-3">
                  <h2 class="accordion-header" id="flush-headingTwo" data-aos="fade-left">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                          data-bs-target="#flush-collapseTwo" aria-expanded="false"
                          aria-controls="flush-collapseTwo">
                          Berapa hari minimal sewa mobil lepas kunci?
                      </button>
                  </h2>
                  <div id="flush-collapseTwo" class="accordion-collapse collapse"
                      aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id
                          dicta eligendi corrupti minima iste fugiat. Quisquam ea nesciunt, rerum commodi
                          quaerat error obcaecati alias non! Consequuntur nam quia consectetur deserunt?</div>
                  </div>
              </div>
              <div class="accordion-item mb-3">
                  <h2 class="accordion-header" id="flush-headingThree" data-aos="fade-left">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                          data-bs-target="#flush-collapseThree" aria-expanded="false"
                          aria-controls="flush-collapseThree">
                          Berapa hari sebelumnya sabaiknya booking sewa mobil?
                      </button>
                  </h2>
                  <div id="flush-collapseThree" class="accordion-collapse collapse"
                      aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                          Quas, harum maxime tempora eius incidunt, vel tempore explicabo consectetur eligendi
                          aperiam distinctio odit odio aut dolor corrupti quisquam inventore est facilis.
                      </div>
                  </div>
              </div>
              <div class="accordion-item mb-3">
                  <h2 class="accordion-header" id="flush-headingFour" data-aos="fade-left">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                          data-bs-target="#flush-collapseFour" aria-expanded="false"
                          aria-controls="flush-collapseFour">
                          Apakah Ada biaya antar-jemput?
                      </button>
                  </h2>
                  <div id="flush-collapseFour" class="accordion-collapse collapse"
                      aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body">Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                          Voluptatibus eaque corrupti numquam nulla necessitatibus, sequi nostrum qui fugiat
                          saepe. Laborum, corporis fuga placeat cum perferendis hic eligendi cupiditate vel
                          laboriosam.</div>
                  </div>
              </div>
              <div class="accordion-item mb-3">
                  <h2 class="accordion-header" id="flush-headingFive" data-aos="fade-left">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                          data-bs-target="#flush-collapseFive" aria-expanded="false"
                          aria-controls="flush-collapseFive">
                          Bagaimana jika terjadi kecelakaan
                      </button>
                  </h2>
                  <div id="flush-collapseFive" class="accordion-collapse collapse"
                      aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                          Provident quidem voluptas ex ducimus culpa facere. Inventore vel rerum aliquam, ipsa
                          sed consectetur id impedit, voluptate sint assumenda, accusantium atque aspernatur!
                      </div>
                  </div>
              </div>
                
          </div>
        </div>
        </div>
      </div>
    </div>
  );
};

export default Faq;
