import Fb from "./images/icon_facebook.png";
import Ig from "./images/icon_instagram.png";
import Twitter from "./images/icon_twitter.png";
import Email from "./images/icon_mail.png";
import Twitch from "./images/icon_twitch.png";
import logo from "./images/Rectangle74.png";

const Footer = () => {
  return (
    <footer class="footer">
      <div class="container">
          <div class="row">
              <div class="col-md-4 text-left">
                <p>Jalan Suroyo No. 161 Mayangan Kota<br/>Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
              </div>
              <div class="col-md-2">
                <a href="#services" class="linkFooter mb-2">Our services</a>
                <a href="#whyus" class="linkFooter mb-2">Why Us</a>
                <a href="#testimonial" class="linkFooter mb-2">Testimonial</a>
                <a href="#faq" class="linkFooter mb-2">FAQ</a>
                
              </div>
              <div class="col-md-3 text-left">
                <p>Connect with us</p>
                <div class="d-flex">
                  <img src={Fb} alt="fb" class="me-3"/>
                  <img src={Ig} alt="ig" class="me-3"/>
                  <img src={Twitter} alt="twitter" class="me-3"/>
                  <img src={Email} alt="email"  class="me-3"/>
                  <img src={Twitch} alt="Twitch"/>
                </div>
              </div>
              <div class="col-md-3">
                <p>Copyright Binar 2022</p>
                <img src={logo} alt="kotak"/>
              </div>             
          </div>
      </div>
    </footer>
  );
};

export default Footer;
