import like from "./images/1.png";
import tag from "./images/2.png";
import jam from "./images/3.png";
import bros from "./images/4.png";

const WhyUs = () => {
  return (
    <div class="whyus" id="whyus">
      <div class="container">        
          <div class="mg-b-35">
              <h1 class="mg-b-0">why us ?</h1>
              <span>Mengapa harus pilih Binar Car Rental?</span>
          </div> 
          <div class="row">
              <div class="col-md-3 mb-md-0 mb-5">
                  <div class="why-us content">
                      <img src={like} alt="like" class="mb-3"/>
                      <h5>Mobil Lengkap</h5>
                      <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                  </div>
              </div>
              <div class="col-md-3 mb-md-0 mb-5">
                <div class="why-us content">
                    <img src={tag} alt="tag" class="mb-3"/>
                    <h5>Harga Murah</h5>
                    <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                </div>
                </div>
                <div class="col-md-3 mb-md-0 mb-5">
                  <div class="why-us content">
                      <img src={jam} alt="jam" class="mb-3"/>
                      <h5>Layanan 24 Jam</h5>
                      <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                  </div>
                </div>
                <div class="col-md-3 mb-md-0 mb-5">
                  <div class="why-us content">
                      <img src={bros} alt="bros"  class="mb-3"/>                      
                      <h5>Sopir Profesional</h5>
                      <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                  </div>
                </div>
          </div>
      </div>
  </div>
  );
};

export default WhyUs;
