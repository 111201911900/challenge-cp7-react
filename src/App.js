import 'bootstrap/dist/css/bootstrap.min.css';
import {Routes, Route} from "react-router-dom";
import Page from "./components/indexpage";
import CarsPage from './components/indexcars';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Page/>} />
        <Route path="/cars" element={<CarsPage/>} />
      </Routes>
    </div>
  );
}

export default App;
