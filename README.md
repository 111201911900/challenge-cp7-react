# Binar Challenge Cp-7
di buat oleh sayyida Amira Muthia Dina

## cara menjalankan
1. cloning project di dalam folder yang di inginkan mengunakan terminal dan perintah dibawah
```properties
git clone https://gitlab.com/111201911900/challenge-cp7-react.git
```

2. instal semua dependensi yang di butuhkan mengunakan perintah di bawah

```properties
    yarn install
```

3. nyalakan server dengan perintah di bawah
```properties
    npx json-server --watch db/cars.json --port 8080
```

4. buka terminal baru dan masuk ke folder file `challenge-cp7-react`


5. lalu masukan perintah di bawah untuk menjalankan 

```properties
    yarn start
```

6. klik mulai sewa mobil untuk masuk kehalaman cars dan klik persegi biru di pojok kiri untuk kembali ke halaman awal 
7. anda dapat menfilter mobil di tanggal 23 maret di jam 15.00
